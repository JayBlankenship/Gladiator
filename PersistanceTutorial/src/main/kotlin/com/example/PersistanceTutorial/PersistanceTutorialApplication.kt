package com.example.PersistanceTutorial

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.web.bind.annotation.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@SpringBootApplication
class PersistanceTutorialApplication

fun main(args: Array<String>) {
	runApplication<PersistanceTutorialApplication>(*args)
}


interface PlayerDataRepository: JpaRepository<player, Long>
{
	@Query(value = "call UserNamePro(:UID)",nativeQuery = true)//native to mysql
	fun UserNamePro(@Param("UID") UID:String): player

	@Query(value = "call deleteplayer(:UID,:UPW)",nativeQuery = true)//literial stored procedure
	fun deleteplayer(@Param("UID") UID:String,@Param("UPW") UPW:String): player
	//@Param:("usern") usern:String,@Param:("ppassword") ppassword:String



}

interface ItemPlayerDataRepository: JpaRepository<player_item_amount, Long> {

	@Query(value = "call CheckForUpdate(:UID,:IID)",nativeQuery = true)//literial stored procedure
	fun CheckForUpdate(@Param("UID") UID:String,@Param("IID") IID:String): player_item_amount

	@Query(value = "call UpdatePlayerinreltable(:UID, :IID, :A)",nativeQuery = true)//literial stored procedure
	fun UpdatePlayerinreltable(@Param("UID") UID:String,@Param("IID") IID:String,@Param("A") A: String): player_item_amount

	@Query(value = "call AddPlayerinreltable(:UID, :IID, :A)",nativeQuery = true)//literial stored procedure
	fun AddPlayerinreltable(@Param("UID") UID:String,@Param("IID") IID:String,@Param("A") A: String): player_item_amount

}

interface ItemDataRepository: JpaRepository<item, Long> {

}
@RestController
@RequestMapping("api")
class PlayerDataRestController(val PlayerDataRepo: PlayerDataRepository,val ItemDataRepo: ItemDataRepository,val ItemPlayerDataRepo: ItemPlayerDataRepository)
{


	@GetMapping("PlayerDataDelete/{UID}/{UPW}")
	fun deletep(@PathVariable(value = "UID") UID: String, @PathVariable(value = "UPW")UPW:String):player
	{
		return PlayerDataRepo.deleteplayer(UID,UPW)
	}
	@GetMapping("playerdata")
	fun GetAll() : List<player>
	{
		return PlayerDataRepo.findAll()
	}
	@GetMapping("PlayerDatatester/{UID}")
	fun UserPro(@PathVariable(value = "UID") UID : String) : player
	{
		return PlayerDataRepo.UserNamePro(UID)
	}

	@PostMapping("Playerdata")
	fun SavePlayerData(@RequestBody Player:player)
	{
		PlayerDataRepo.save(Player)
	}

	@GetMapping("itemdata")
	fun GetAllitems() : List<item>
	{
		return ItemDataRepo.findAll()
	}
	@GetMapping("PlayerDatalookupupdate/{UID}/{IID}")
	fun CheckUpdate(@PathVariable(value = "UID") UID : String,@PathVariable(value = "IID") IID : String): player_item_amount
	{
		return ItemPlayerDataRepo.CheckForUpdate(UID,IID)
	}
	@GetMapping("UpdatePlayerinreltable/{UID}/{IID}/{A}")
	fun MakeUpdate(@PathVariable(value = "UID") UID : String,@PathVariable(value = "IID") IID : String,@PathVariable(value = "A") A : String): player_item_amount
	{
		return ItemPlayerDataRepo.UpdatePlayerinreltable(UID,IID,A)
	}
	@GetMapping("AddPlayerinreltable/{UID}/{IID}/{A}")
	fun InsertUpdte(@PathVariable(value = "UID") UID : String,@PathVariable(value = "IID") IID : String,@PathVariable(value = "A") A : String): player_item_amount
	{
		return ItemPlayerDataRepo.AddPlayerinreltable(UID,IID,A)
	}

}

@Entity
class player(
		@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
		val playerid: Long = 0,val username: String, val email: String, val password: String, val isvalid: Boolean = false
		)

@Entity
class player_item_amount(
		@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
		val playerid: Long = 0,val itemid: Long = 0,val amount: Long = 0
)

@Entity
class item(
		@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
		val itemid: Long = 0,val itemname: String,val description: String, val value: Long = 0, val requiredlevel: Long = 0
)
